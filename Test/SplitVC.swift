//
//  SplitVC.swift
//  Test
//
//  Created by Konstantin Chukhas on 18.10.2019.
//  Copyright © 2019 Konstantin Chukhas. All rights reserved.
//

import UIKit

class SplitVC: UISplitViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        self.preferredDisplayMode = UISplitViewController.DisplayMode.allVisible
        self.maximumPrimaryColumnWidth = 250
        
        
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
