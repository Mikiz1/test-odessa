
import UIKit

class MainCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var img: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    
    override func prepareForReuse() {
        super.prepareForReuse()
        img.image = nil
        nameLabel.text = nil
        timeLabel.text = nil
        descriptionLabel.text = nil
    }
}
