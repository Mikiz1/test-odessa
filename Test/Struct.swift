
import Foundation
struct User: Codable {
    var name:String
    var image:String
    var description:String
    var time:String
    var itemId:String
    private enum CodingKeys: String, CodingKey {
        case name
        case image
        case description
        case time
        case itemId
        
    }
}
struct CollectionStruct: Codable {
    var name:String
    var image:String
    var description:String
    var time:String
    var itemId:String
    private enum CodingKeys: String, CodingKey {
        case name
        case image
        case description
        case time
        case itemId
        
    }
}
