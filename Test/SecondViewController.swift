
import UIKit

class SecondViewController: UIViewController {
    @IBOutlet weak var menu: UIBarButtonItem!
    @IBOutlet weak var collectionView: UICollectionView!
    var arr = [User]()
    var collectionStr = [CollectionStruct]()
    var cs:User?
    var passedValueName = ""
    var passedValueDescription = ""
    var passedValueTime = ""
    var passedValueImage = ""
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        collectionView.delegate = self
        collectionView.dataSource = self
        NetworkManager.sharedInstance.downloadJSON { (user) in
            self.arr.append(user as! User)
            DispatchQueue.main.async {
                self.collectionView.reloadData()
            }
        }
        arr.append(User(name: passedValueName, image: passedValueImage, description: passedValueDescription, time: passedValueTime, itemId: "0"))
        collectionView.reloadData()
        
    }
   
    
    
}
extension SecondViewController:UICollectionViewDelegate, UICollectionViewDataSource{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        let ar = arr.uniqueValues(value: {$0.name})
        return ar.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as? MainCollectionViewCell
        
        let ar = arr.uniqueValues(value: {$0.name})
        let array = ar[indexPath.row]
        cell?.nameLabel.text = array.name
        cell?.descriptionLabel.text = array.description
        cell?.img.downloadImage(from: array.image)
        let date = Date(timeIntervalSince1970: TimeInterval(Int(array.time) ?? 0))
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MMM dd YYYY hh:mm a"
        let localDate = dateFormatter.string(from: date)
        cell?.timeLabel.text = localDate
        return cell!
    }
    
    
    
}
extension SecondViewController:UICollectionViewDelegateFlowLayout{
    
    private func collectionView(collectionView: UICollectionView,
       layout collectionViewLayout: UICollectionViewLayout,
       insetForSectionAtIndex section: Int) -> UIEdgeInsets {

        let cellWidth : CGFloat = 110;

        let numberOfCells = floor(self.view.frame.size.width / cellWidth);
        let edgeInsets = (self.view.frame.size.width - (numberOfCells * cellWidth)) / (numberOfCells + 1);

        return UIEdgeInsets(top: 0, left: edgeInsets, bottom: 60.0, right: edgeInsets);
   }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let size = collectionView.frame.size
        return CGSize(width:size.width , height: size.height )
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
}


