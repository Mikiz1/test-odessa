

import Foundation
import UIKit
extension Array
{
    func uniqueValues<V:Equatable>( value:(Element)->V) -> [Element]
    {
        var result:[Element] = []
        for element in self
        {
            if !result.contains(where: { value($0) == value(element) })
            { result.append(element) }
        }
        return result
    }
}
