
import UIKit

enum selectedScope:Int{
    case name = 0
    case description = 1
}
class MainViewController: UIViewController {
    
    
    @IBOutlet weak var tableView: UITableView!
    private var arr = [User]()
    var valueToPassName:String!
    var valueToPassDescription:String!
    var valueToPassTime:String!
    var valueToPassImage:String!
    
    
    fileprivate func getArray() {
        NetworkManager.sharedInstance.downloadJSON { (user) in
            self.arr.append(user as! User)
            DispatchQueue.main.async {
                self.arr = self.arr.uniqueValues(value: {$0.name})
                self.tableView.reloadData()
                
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        searchController.searchBar.isTranslucent = true
        navigationItem.searchController = searchController
        searchController.searchBar.delegate = self
        tableView.dataSource = self
        tableView.delegate = self
        getArray()
    }
    private let searchController: UISearchController = {
        
        let searchController = UISearchController(searchResultsController: nil)
        searchController.searchBar.translatesAutoresizingMaskIntoConstraints = false
        searchController.searchBar.accessibilityIdentifier = "SearchBar"
        searchController.searchBar.placeholder = "Search"
        searchController.searchBar.tintColor = #colorLiteral(red: 0.8360546875, green: 0.8360546875, blue: 0.8360546875, alpha: 1)
        searchController.searchBar.barStyle = .black
        searchController.searchBar.sizeToFit()
        searchController.searchBar.enablesReturnKeyAutomatically = true
        searchController.hidesNavigationBarDuringPresentation = true
        searchController.searchBar.returnKeyType = .done
        searchController.searchBar.showsScopeBar = true
        searchController.searchBar.scopeButtonTitles = ["Name","Description"]
        searchController.searchBar.selectedScopeButtonIndex = 0
        
        return searchController
        
    }()
    
}
extension MainViewController:UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arr.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as? MainTableViewCell
        let array = arr[indexPath.row]
        cell?.nameLabel.text = array.name
        cell?.descriptionLabel.text = array.description
        cell?.img.downloadImage(from: array.image)
        let date = Date(timeIntervalSince1970: TimeInterval(Int(array.time)!))
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = " dd MMM  hh:mm a"
        let localDate = dateFormatter.string(from: date)
        cell?.timeLabel.text = localDate
        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("You selected cell #\(indexPath.row)!")
        
        let indexPath = tableView.indexPathForSelectedRow!
        let currentCell = tableView.cellForRow(at: indexPath)! as! MainTableViewCell
        let array = arr[indexPath.row]
        valueToPassName = currentCell.nameLabel.text
        valueToPassTime = currentCell.timeLabel.text
        valueToPassDescription = currentCell.descriptionLabel.text
        valueToPassImage = array.image
        performSegue(withIdentifier: "push", sender: self)
        
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if let nav = segue.destination as? UINavigationController,
            let vc = nav.topViewController as? SecondViewController {
            vc.passedValueName = valueToPassName
            vc.passedValueDescription = valueToPassDescription
            vc.passedValueTime = valueToPassTime
            vc.passedValueImage = valueToPassImage
            
        }
    }
    
}



extension MainViewController: UISearchBarDelegate {
    func searchBar(_ searchBar: UISearchBar, selectedScopeButtonIndexDidChange selectedScope: Int) {
        
        if selectedScope == 0 {
            arr = arr.sorted(by: { $0.name < $1.name })
            tableView.reloadData()
        }
        else if selectedScope == 1 {
            arr = arr.sorted(by: { $0.description < $1.description })
            tableView.reloadData()
        }
    }
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        if searchText.isEmpty {
            getArray()
        }else{
            let searchText = searchController.searchBar.text!
            arr = arr.filter { $0.name.range(of: searchText, options: [.literal]) != nil
                || $0.description.range(of: searchText, options: [.literal]) != nil }
        }
        tableView.reloadData()
    }
}


extension Date {
    var millisecondsSince1970:Int64 {
        return Int64((self.timeIntervalSince1970 * 1000.0).rounded())
    }

    init(milliseconds:Int64) {
        self = Date(timeIntervalSince1970: TimeInterval(milliseconds) / 1000)
    }
}
